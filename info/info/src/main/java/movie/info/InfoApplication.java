package movie.info;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class InfoApplication {

    @RequestMapping("/eureka-client-2")
    public String home() throws JSONException {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("message", "Hello World 1");
        jsonObject.put("message-3", "test");
        return jsonObject.toString();
    }


    public static void main(String[] args) {
        SpringApplication.run(InfoApplication.class, args);
    }

}
