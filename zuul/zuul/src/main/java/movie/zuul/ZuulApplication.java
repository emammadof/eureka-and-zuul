package movie.zuul;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@SpringBootApplication
@EnableZuulProxy
@EnableEurekaClient
@RestController
public class ZuulApplication {


    @RequestMapping("/eureka-client-1")
    public String home() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("message", "Hello World ");
        jsonObject.put("message-2", "test");
        return jsonObject.toString();
    }

    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication.class, args);
    }

    @Bean
    public ZuulFilter postFilter() {
        return new ZuulFilter() {

            @Override
            public boolean shouldFilter() {
                return true;
            }

            @Override
            public Object run() throws ZuulException {
                System.out.println("Pre Filter-	run");
                HttpServletRequest request = RequestContext.getCurrentContext().getRequest();
                System.out.println("Request Method: " + request.getMethod());
                System.out.println("Request URL: " + request.getRequestURL().toString());
                return null;
            }

            @Override
            public String filterType() {
                return "pre";
            }

            @Override
            public int filterOrder() {
                return 1;
            }
        };
    }

}
